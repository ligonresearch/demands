ORG_INPUTS = demands.org class.org \
             engel_curves.org    \
             monotone_function_solver.org \
	     class.org

.PHONY: tangle wheel upload localinstall devinstall clean test

all: tangle test devinstall wheel

tangle: .tangle

.tangle: $(ORG_INPUTS) 
	(./tangle.sh demands.org)
	(./tangle.sh engel_curves.org)
	(./tangle.sh monotone_function_solver.org)
	#(./tangle.sh class.org)
	touch .tangle

test: .test 

.test: $(ORG_INPUTS)
	pytest consumerdemands/test/
	touch .test

wheel: setup.py tangle test CHANGES.txt demands/requirements.txt
	pip wheel --wheel-dir=dist/ .

CHANGES.txt:
	git log --pretty='medium' > CHANGES.txt

demands/requirements.txt:
	(cd consumerdemands; pigar)

localinstall: clean wheel
	(cd dist; pip install Demands*.whl --upgrade)

devinstall: tangle test 
	pip install -e .

upload: wheel
	twine upload dist/*

clean: 
	-rm -f dist/*.tar.gz dist/*.exe dist/*.whl
	-rm -f demands/requirements.txt
	-rm -f CHANGES.txt
	-rm -f .test
	-rm -f .tangle
