
==============
Demand Systems
==============

This repository contains code for describing a general set of demand systems.

INSTALLATION
============

The preferred installation method involves using ``pip``.  If you don't
have this  installed (or if your install is old), see
https://pip.pypa.io/en/stable/installing/
  
  1. With pip::

        pip install Demands

  2. As a python package: Unzip/untar the relevant archive (which can
     be downloaded from https://pypi.python.org/pypi/Demands/), then::

        cd Demands-[version]/
        sudo python setup.py install

  3. Using git: Clone the repository found at
     https://bitbucket.org/ligonresearch/demands to someplace in
     your filesystem, e.g. (for user jrandom, on a machine with user
     directories under /home/)::

        git clone git@bitbucket.org:ligonresearch/demands.git /home/jrandom/Demands



   




  
