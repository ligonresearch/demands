# -*- org-src-preserve-indentation: t; -*-
:SETUP:
#+TITLE: ConsumerDemands Class
#+AUTHOR: Ethan Ligon
#+OPTIONS: toc:nil
#+PROPERTY: header-args:python :results output :noweb no-export :exports code :comments link :prologue (format "# Tangled on %s" (current-time-string))
#+LATEX_HEADER: \renewcommand{\vec}[1]{\boldsymbol{#1}}
#+LATEX_HEADER: \newcommand{\T}{\top}
#+LATEX_HEADER: \newcommand{\E}{\ensuremath{\mbox{E}}}
#+LATEX_HEADER: \newcommand{\R}{\ensuremath{\mathbb{R}}}
#+LATEX_HEADER: \newcommand{\Cov}{\ensuremath{\mbox{Cov}}}
#+LATEX_HEADER: \newcommand{\Eq}[1]{(\ref{eq:#1})}
#+LATEX_HEADER: \newcommand{\Fig}[1]{Figure \ref{fig:#1}} \newcommand{\Tab}[1]{Table \ref{tab:#1}}
#+LATEX_HEADER: \renewcommand{\refname}{}
#+LATEX_HEADER: \usepackage{stringstrings}\renewcommand{\cite}[1]{\caselower[q]{#1}\citet{\thestring}}
:END:

For a general utility function, solve maximization problem numerically
to define aspects of Frischian demands, which can then be mapped into
other representations of the demand system.

Though quite general, this approach is both expensive and inexact when
compared with cases for which closed form solutions are available.

* Main description of =OracleDemands=

** Private methods of =OracleDemands=
We begin by defining an =__init__= and some private methods for the
class =OracleDemands=. 

#+name: result_class
#+BEGIN_SRC python :noweb no-export :results output :tangle ./consumerdemands/demands_class.py
import numpy as np
import pandas as pd
import warnings
import torch

import cvxpy as cp
from osmm import OSMM
osmm_prob = OSMM()

from .root_with_precision import root_with_precision

class OracleDemands(object):

    """Objects of this class describe demands for a given utility function.

    The supplied utility function is assumed to be increasing,
    concave, and continuously differentiable.  It must map a vector (a
    =pytorch.tensor=) into a float, but otherwise can have any desired structure. 

    There are three fundamental representations of demand described:
       - Marshallian (demands depend on prices & budget)
       - Hicksian (demands depend on prices and a level of utility)
       - Frischian (demands depend on prices and a marginal utility of expenditures (MUE).    
    """

    def __init__(self, utility, tol=1e-12, verbose=False,**kwargs):
        """Supply a function which maps a vector represented as a
        =torch.tensor= into a float, say U(c).
        """

        self.utility = utility
        self.verbose = verbose
        self.tol = tol

    def _frischian_demands(self,p,lbda):
        """Compute Frischian demands."""

        assert lbda>0,"lambda must be positive."

        osmm_prob.f_torch.function = lambda c: -self.utility(c)

        p = np.array(p)
        c = cp.Variable(len(p), nonneg=True)

        osmm_prob.g_cvxpy.variable = c
        osmm_prob.g_cvxpy.objective = lbda*p@c
        osmm_prob.g_cvxpy.constraints = []

        guess = 1/(lbda*p)

        result = osmm_prob.solve(guess,verbose=self.verbose,eps_gap_abs=self.tol)

        return c.value

    def _marshallian_demands(self,p,x):
        """Compute Marshallian demands."""

        assert x>0,"budget must be positive."

        osmm_prob.f_torch.function = lambda c: -self.utility(c)

        p = np.array(p)
        c = cp.Variable(len(p), nonneg=True)

        osmm_prob.g_cvxpy.variable = c
        osmm_prob.g_cvxpy.objective = 0
        osmm_prob.g_cvxpy.constraints = [p@c==x]

        guess = x/(len(p)*p)

        result = osmm_prob.solve(guess,verbose=self.verbose,eps_gap_abs=self.tol)

        #print("Result:",result)

        assert c.value is not None

        return c.value

    def _hicksian_demands(self,p,U):
        """Compute Hicksian demands."""

        def _sum(x,y):
            S = 0
            for i in range(len(x)):
                S += x[i]*y[i]

            return S

        osmm_prob.f_torch.function = lambda c: _sum(c,p)

        c = cp.Variable(len(p), nonneg=True)

        osmm_prob.g_cvxpy.variable = c
        osmm_prob.g_cvxpy.objective = 0
        osmm_prob.g_cvxpy.constraints = [-self.utility(c)<=-U]

        guess = np.ones(len(p))

        result = osmm_prob.solve(guess,verbose=self.verbose,eps_gap_abs=self.tol)

        print("Result:",result)

        assert c.value is not None

        return c.value

    def _frischian_V(self,p,lbda):
        """Frischian indirect utility."""
        c = self._frischian_demands(p,lbda)
        return self.utility(c)

    def _marshallian_V(self,p,x):
        """Marshallian indirect utility."""
        c = self._marshallian_demands(p,x)
        return self.utility(c)

    def _frischian_x(self,p,lbda):
        """Frischian expenditures."""
        return p@self._frischian_demands(p,lbda)
#+end_src

** Methods describing the utility function

#+begin_src python :noweb no-export :results output :tangle ./consumerdemands/demands_class.py
    def marginal_utilities(self,c):

        # Compute gradient
        x = torch.tensor(c,requires_grad=True,dtype=torch.float)

        return torch.autograd.functional.jacobian(self.utility,x)

    def hessian_utility(self,c):

        # Compute gradient
        x = torch.tensor(c,requires_grad=True,dtype=torch.float)

        return torch.autograd.functional.hessian(self.utility,x)

#+end_src

** Methods describing demands and their primitives

#+begin_src python :noweb no-export :results output :tangle ./consumerdemands/demands_class.py
    def lambdavalue(self,p,x=None,U=None,ub=10,tol=1e-12):
        """
        Given expenditures x, prices p return the MUE, lambda(p,x);
        ,*or* if given level of utility U return lambda(p,U)
        """
        def excess_expenditures(p,x):
            def f(lbda):
                """"Difference in expenditures p@f(lbda,p)-x"""
                #lbda = np.abs(lbda)
                c = self._frischian_demands(p,lbda)
                return p@c-x

            return f

        def excess_utility(p,U):
            def f(lbda):
                """"Difference in expenditures p@f(lbda,p)-x"""
                #lbda = np.abs(lbda)
                V = self._frischian_V(p,lbda)
                return V-U
            return f

        assert not ((x is None)*(U is None)), "Must supply either x or U."

        if x is not None:
            return root_with_precision(excess_expenditures(p,x),[0,ub,np.Inf],tol,open_interval=True)
        elif U is not None:
            return root_with_precision(excess_utility(p,U),[0,ub,np.Inf],tol,open_interval=True)

     
    def demands(self,p,x=None,U=None,lbda=None):
        """
        Demands at prices p, given one of
          - x (Marshallian)
          - U (Hicksian)
          - lbda (Frischian)
        """
        assert ((x is None) + (U is None) + (lbda is None))==2, "Specify one of x,U,lbda."

        if lbda is not None:
            return self._frischian_demands(p,lbda)
        elif x is not None:
            return self._marshallian_demands(p,x)
        elif U is not None:
            try:
                return self._hicksian_demands(p,U)
            except AssertionError:
                lbda = self.lambdavalue(p,U=U)
                return self._frischian_demands(p,lbda)

    def indirect_utility(self,p,x=None,U=None,lbda=None):
        """
        Indirect utility at prices p, given one of
          - x (Marshallian)
          - U (Hicksian)
          - lbda (Frischian)
        """
        assert ((x is None) + (U is None) + (lbda is None))==2, "Specify one of x,U,lbda."

        if lbda is not None:
            return self._frischian_V(p,lbda)
        elif x is not None:
            return self._marshallian_V(p,x)
        elif U is not None:
            return U

    V = indirect_utility

    def expenditures(self,p,x=None,U=None,lbda=None):
        """
        Expenditures at prices p, given one of
          - x (Marshallian)
          - U (Hicksian)
          - lbda (Frischian)
        """
        assert ((x is None) + (U is None) + (lbda is None))==2, "Specify one of x,U,lbda."

        if lbda is not None:
            return self._frischian_x(p,lbda)
        elif x is not None:
            return x
        elif U is not None:
            return p@self.demands(p,U=U)

#+end_src

#+begin_src python :tangle ./consumerdemands/test/test_oracle.py
from consumerdemands import OracleDemands
import numpy as np
from cfe import demands
import torch

def utility(x,alpha,beta,phi):
    """
    Direct utility from consumption of x.
    """
    def _utility(x):
        n = len(alpha)
        
        U=0
        for i in range(n):
            if beta[i]==1:
                U += alpha[i]*torch.log(x[i]+phi[i])
            else:
                U += alpha[i]*((x[i]+phi[i])**(1-1./beta[i])-1)*beta[i]/(beta[i]-1)
            if i==n-1:
                b = np.mean(beta)
                U += (x[i]+x[i-1])**(1-1./b)*b/(b-1)

        return U

    return _utility(x)


n = 5
alpha = [i+1 for i in range(n)]
beta = [2/a for a in alpha]
beta0 = [1/a+1 for a in alpha]
beta1 = [5/(3*a) for a in alpha]

phi = [0]*n


# Instantiate demands object
#d = OracleDemands(lambda c: utility(c,alpha,beta,phi))
d = OracleDemands(lambda c: utility(c,alpha,beta0,phi) + utility(c,alpha,beta1,phi))

# Evaluate gradient at this point
c = np.ones(len(alpha))

m = d.marginal_utilities(c)

h = d.hessian_utility(c)

print("Marginal utilities: ",m)

p = [1]*n
c = d._frischian_demands(p,3)

print("Demands: ",c)

print("lambda: ",d.lambdavalue(p,3))

#+end_src

* Demonstration of =OracleDemands=
  :PROPERTIES:
  :EXPORT_FILE_NAME: oracle_demands_demo.ipynb
  :END:

This is meant to describe and demonstrate the uses of a class
=OracleDemands= for modeling consumer demands in a quite general way.

** Preface

We begin by importing the tools we need.  Note that much of the heavy
lifting is accomplished using the =PyTorch= framework, which allows
for the rapid calculation of gradients, and scales nicely using GPUs
or clusters.

#+begin_src ipython :tangle /tmp/oracle_demo.py
from consumerdemands import OracleDemands
import numpy as np
from cfe import demands
import torch

#+end_src

** Consumer Utility
Suppose we begin with a consumer's utility function
$U:\Re^n\rightarrow\Re$, assumed to be strictly increasing, strictly
concave, and continuously differentiable.

Here we build a non-homothetic, non-separable utility function over
$n$ goods.  But any utility function in the class we've talked about
should work, including utility functions which  may not have a simple
closed-form expression.
#+begin_src ipython :tangle /tmp/oracle_demo.py

def utility(c,alpha,beta,phi):
    """
    Direct utility from consumption of c.
    """
    def _utility(c):
        n = len(alpha)
        
        U=0
        for i in range(n):
            if beta[i]==1:
                U += alpha[i]*torch.log(c[i]+phi[i])
            else:
                U += alpha[i]*((c[i]+phi[i])**(1-1./beta[i])-1)*beta[i]/(beta[i]-1)

        b = np.mean(beta)

        if n>2:
            U += c[2]*(np.linalg.norm([c[1],c[0]],1.1)**(1-1./b)*b/(b-1) # Build in "specific substitution" effect

        return U

    return _utility(c)

n = 10
alpha = [i+1 for i in range(n)]
beta0 = [1/a+1 for a in alpha]
beta1 = [5/(3*a) for a in alpha]

phi = [0]*n


# Instantiate demands object
#d = OracleDemands(lambda c: utility(c,alpha,beta,phi))

# Complicate utility further, by adding up two utility functions with different curvature parameters.
myU = lambda c: utility(c,alpha,beta0,phi) + utility(c,alpha,beta1,phi)
#+end_src

Let's take a look at indifference curves between the first two goods $(c_0,c_1)$,
and allowing us to tweak the third good ($c_2$).

#+begin_src ipython
import plotly.graph_objects as go
import numpy as np

U = lambda c0,c1,c2=1,cbar=[2,3]: myU([c0,c1,c2]+cbar)

c0 = np.linspace(0.1, 4, 30)
c1 = np.linspace(0.1, 4, 30)

data = [dict(type='surface',
             visible=False,
             z = np.array([[U(c_0,c_1,c2) for c_0 in c0] for c_1 in c1]),
             x = c0,
             y = c1,
             name = '$c_2=%f$' % c2)
        for c2 in [.1,.2,.3,.4,.5]]


# Create figure
fig = go.Figure(data=data)

fig.data[0].visible=True

# Create and add slider
c2s = []
for i in range(len(fig.data)):
    c2 = dict(
        method="update",
        args=[{"visible": [False] * len(fig.data)},
              {"title": "c_2=%s " % str(i)}],  # layout attribute
    )
    c2["args"][0]["visible"][i] = True  # Toggle i'th trace to "visible"
    c2s.append(c2)

sliders = [dict(
    active=0,
    currentvalue={"prefix": "$c_2$: "},
    pad={"t": 50},
    steps=c2s
)]

fig.update_layout(
    sliders=sliders
)

fig.update_traces(contours_z=dict(show=True, usecolormap=True,
                                  highlightcolor="limegreen", project_z=True))

fig.update_layout(title='Utility', autosize=False,
                  scene_camera_eye=dict(x=1.87, y=0.88, z=-0.64),
                  width=500, height=500,
                  margin=dict(l=65, r=50, b=65, t=90))

fig.show()
#+end_src

** Instantiate demand system

If we fix a utility function, we fix a demand system.  
#+begin_src ipython :tangle /tmp/oracle_demo.py

d = OracleDemands(myU,verbose=True)
#+end_src

Now, our secret sauce is being able to rapidly compute gradients of
the unknown utility function, so let's try that:
#+begin_src ipython :tangle /tmp/oracle_demo.py
# Evaluate gradient at this point
c = np.ones(len(alpha))

d.marginal_utilities(c)

#+end_src

We can also rapidly compute second derivatives.  Here's the Hessian of
the direct utility function:
#+begin_src ipython :tangle /tmp/oracle_demo.py
H=d.hessian_utility(c)
H
#+end_src

Note that this Hessian is proportional to the inverse Frischian
substitution matrix, which gives what Theil calls "specific
substitution" effects.
#+begin_src ipython
F = np.linalg.inv(H)
F
#+end_src

One way of characterizing the complexity of substitution between goods
in the demand system is to describe the rank of the matrix $F$ after
subtracting its diagonal terms:
#+begin_src ipython
np.linalg.matrix_rank(F-np.diag(np.diag(F)))
#+end_src

** Demands
That's enough of our exploration of the direct utility function.  With
the utility function we can also compute demands in any of several
representations.   See documentation of the demand method:
#+begin_src ipython
d.demands?
#+end_src

Now fix some prices, and compute Marshallian demands
#+begin_src ipython :tangle /tmp/oracle_demo.py
p = [1]*n
m = d.demands(p,x=3) # x is our budget

m
#+end_src
At these demands we also have direct utility:
#+begin_src ipython
d.utility(m)
#+end_src

Compositions of With Marshallian demands with the utility function are
just called the indirect utility function, or
#+begin_src ipython :tangle /tmp/oracle_demo.py
u=d.V(p,x=3)

u
#+end_src

Similarly, Hicksian demands just involve fixing the level of utility
instead of the budget:
#+begin_src ipython :tangle /tmp/oracle_demo.py
#h = d.demands(p,U=u)

#h
#+end_src
