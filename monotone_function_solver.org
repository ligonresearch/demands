:SETUP:
#+TITLE: Algorithm to find the root of a monotone function
#+DATE: <2013-10-21 Mon>
#+AUTHOR: Ethan Ligon
#+EMAIL: ligon@berkeley.edu
#+PROPERTY: header-args:python :results output :noweb no-export :exports code :comments link :prologue (format "# Tangled on %s" (current-time-string))
#+OPTIONS: ':t *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline author:t
#+OPTIONS: c:nil creator:comment d:(not LOGBOOK) date:t e:t email:nil
#+OPTIONS: f:t inline:t num:t p:nil pri:nil stat:t tags:t tasks:t
#+OPTIONS: tex:t timestamp:t toc:t todo:t |:t
#+CREATOR: Emacs 24.3.50.1 (Org mode 8.1.2)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: en
#+SELECT_TAGS: export
#+OPTIONS: texht:t
#+LATEX_CLASS: amsart
#+LATEX_CLASS_OPTIONS:
#+LATEX_HEADER: \newcommand{\R}{\mathbb{R}}
:END:

Consider a function $f:\R\rightarrow\R$.  The function is continuous, monotonic, and is known
to have a root $x_0$ in a bounded interval $[a,b]$.  

One approach to finding the root of a monotonic function relies
on the mean value theorem, and is guaranteed to find the root.  Such
an algorithm involves simply dividing an interval into two parts at each step,
and at each step testing which part the root lies in.

The following iterator maps a triple $x$ into another triple.  The first
and last elements of $x$ are the endpoints of the interval; the
initial $x$ may be only a pair.  A function =middle= determines how
the next point to be evaluated within the interval should be chosen.
#+name: root_interval
#+begin_src python :exports code
from numpy import sign, Inf
from warnings import warn

def root_interval(f,x,middle,fp=None):
    """
    f is a monotonic function defined over an interval (x[0],x[-1]).
    x is a pair or a triple such that the root of f is within that same interval.
    middle is a function mapping a function and an interval into a point within the interval.
    """ 
    if len(x)==2: # If only endpoints provided supply an interior guess
        x=[x[0],middle(f,x,fp),x[1]]
        yield x

    y=[f(z) for z in x]
    while abs(x[0]-x[-1])>0:
        if y[0]==0.: # Found the root!
            x=[x[0]]*3
            yield x
        elif y[1]==0.:
            x=[x[1]]*3
            yield x
        elif y[-1]==0.:
            x=[x[-1]]*3
            yield x
        elif sign(y[0])==sign(y[-1]):
            raise ValueError("No root in interval (%f,%f) or function not monotone." % (x[0],x[-1]))
        elif sign(y[0])==sign(y[1]): # Root must be in upper sub-interval
            x=[x[1],middle(f,[x[1],x[-1]],fp),x[-1]]
            y=[y[1],f(x[1]),y[-1]]
            yield x
        elif sign(y[1])==sign(y[2]): # Root must be in lower sub-interval
            x=[x[0],middle(f,[x[0],x[1]],fp),x[1]]
            y=[y[0],f(x[1]),y[1]]
            yield x
        else:
            print(x,y)
            raise AssertionError("Something impossible happened.")
#+end_src

How is the middle point to be chosen?  We discuss three possibilities.

* Bisection
The simplest approach is simply to divide the interval in half; the following =middle=
function returns a point in the middle of an interval.
#+name: bisect
#+begin_src python :noweb no-export :exports code
def middle(f,x,fp):
    return (x[0]+x[-1])/2.

<<root_interval>>
#+end_src

#+name: bisect_example
#+begin_src python :noweb no-export :exports results :results output
<<bisect>>

f=lambda x: 1./x-1
seq=root_interval(f,[1e-3,1e3],middle=middle)
for i in range(20):
    x=next(seq)
    print i,x[1]
#+end_src

#+results: bisect_example
#+begin_example
0 500.0005
1 250.00075
2 125.000875
3 62.5009375
4 31.25096875
5 15.625984375
6 7.8134921875
7 3.90724609375
8 1.95412304687
9 0.977561523437
10 1.46584228516
11 1.2217019043
12 1.09963171387
13 1.03859661865
14 1.00807907104
15 0.992820297241
16 1.00044968414
17 0.996634990692
18 0.998542337418
19 0.99949601078
#+end_example

When a middle point is chosen by using a simple bisection of this
sort, then the number of steps required to find the root to the
required precision =eps= doesn't depend on the function at all; only
on the size of the interval $[a,b]$ and the precision =eps=, with the
number of steps the smallest integer $n$ satisfying \[
n>\log_2(|a-b|)-\log_2\mbox{eps}.  \] So, for example, if
$[a,b]=[0,1]$ and =eps= is $2^{-63}$, then exactly 64 recursions will
be required to compute the solution.
* Line
The bisection routine divides an interval into half at each step,
taking an average of the endpoints.  But
an algorithm which uses some information about how far $f(a)$ and
$f(b)$ are from zero may converge more quickly.  The following
=middle= function evaluates the function $f$ at both endpoints, finds
the point at which a line passing through through $f(a)$ and $f(b)$
would have a zero, and defines subintervals using this as the 'middle'
point.
#+name: line
#+begin_src python :noweb no-export :exports code
def middle(f,x,fp):
    return x[0]+f(x[0])/(f(x[0])-f(x[-1]))*(x[-1]-x[0])

<<root_interval>>
#+end_src

When the function $f$ is close to linear the performance of this
algorithm will be superior, as the following example with
$f(x)=1-x^{0.9}$ shows.

#+name: line_example0
#+begin_src python :noweb no-export :exports results :results output 
<<line>>

f=lambda x: 1-x**.9
seq=root_interval(f,[1e-3,1e3],middle=middle)
for i in range(10):
    x = next(seq)
    print i,x[1]
#+end_src

#+results: line_example0

But as one might expect, if $f$ is quite non-linear then convergence
can be very slow.  A very bad case is when $f$ is hyperbolic, with
$f(x)=1/x -1$.

#+name: line_example1
#+begin_src python :noweb no-export :exports results :results output
<<line>>

f=lambda x: 1./x-1
seq = root_interval(f,[1e-2,1e2],middle=middle)
for i in range(100):
    x = next(seq)
    print i,x[1]
#+end_src

#+results: line_example1
#+begin_example
0 99.01
1 98.0299
2 97.059601
3 96.09900499
4 95.1480149401
5 94.2065347907
6 93.2744694428
7 92.3517247484
8 91.4382075009
9 90.5338254259
10 89.6384871716
11 88.7521022999
12 87.8745812769
13 87.0058354641
14 86.1457771095
15 85.2943193384
16 84.451376145
17 83.6168623836
18 82.7906937597
19 81.9727868221
20 81.1630589539
21 80.3614283644
22 79.5678140807
23 78.7821359399
24 78.0043145805
25 77.2342714347
26 76.4719287204
27 75.7172094332
28 74.9700373388
29 74.2303369654
30 73.4980335958
31 72.7730532598
32 72.0553227272
33 71.3447695
34 70.641321805
35 69.9449085869
36 69.255459501
37 68.572904906
38 67.897175857
39 67.2282040984
40 66.5659220574
41 65.9102628368
42 65.2611602085
43 64.6185486064
44 63.9823631203
45 63.3525394891
46 62.7290140942
47 62.1117239533
48 61.5006067138
49 60.8956006466
50 60.2966446401
51 59.7036781937
52 59.1166414118
53 58.5354749977
54 57.9601202477
55 57.3905190452
56 56.8266138548
57 56.2683477162
58 55.7156642391
59 55.1685075967
60 54.6268225207
61 54.0905542955
62 53.5596487526
63 53.034052265
64 52.5137117424
65 51.998574625
66 51.4885888787
67 50.9837029899
68 50.48386596
69 49.9890273004
70 49.4991370274
71 49.0141456571
72 48.5340042006
73 48.0586641586
74 47.588077517
75 47.1221967418
76 46.6609747744
77 46.2043650266
78 45.7523213764
79 45.3047981626
80 44.861750181
81 44.4231326792
82 43.9889013524
83 43.5590123389
84 43.1334222155
85 42.7120879933
86 42.2949671134
87 41.8820174423
88 41.4731972678
89 41.0684652952
90 40.6677806422
91 40.2711028358
92 39.8783918074
93 39.4896078893
94 39.1047118105
95 38.7236646924
96 38.3464280454
97 37.972963765
98 37.6032341273
99 37.237201786
#+end_example

* Newton-Raphson
 The bisection method is robust because it uses no information about
 the function $f$ beyond whether it's positive or negative.  Our
 =line= approach above can be thought of as an application of the
 Newton-Raphson approach when the function $f$ is linear, so an
 obvious alternative approach involves using local information on
 derivatives instead of finding the zero of a line defined by
 $(f(a),f(b))$.

 However, the usual Newton-Raphson approach instead uses information
 on the derivative of the function $f$, but no information on the
 bounds $a,b$, and is not guaranteed to converge.  The following code
 combines information from both sources, using information on
 derivatives so long as the Newton-Raphson iterate lies within the
 $[a,b]$ interval, and relying on finding a "middle" using bisection
 when it does not.

 #+name: newton
 #+begin_src python :noweb no-export :exports code
def middle(f,x,fp):
    if len(x)==2:
        return (x[0]+x[-1])/2.
    else:
        xp=f(x[1])/fp(x[1])
        if x[0]<xp<x[-1]:
            return xp
        else:
            return (x[0]+x[-1])/2.

<<root_interval>>
 #+end_src

#+name: newton_example0
#+begin_src python :noweb no-export :exports results :results output
<<newton>>

f=lambda x: 1./x-1
seq=root_interval(f,[1e-2,1e2],middle=middle)
for i in range(25):
    x=seq.next()
    print i,x[1]
#+end_src

#+results: newton_example0
#+begin_example
0 50.005
1 25.0075
2 12.50875
3 6.259375
4 3.1346875
5 1.57234375
6 0.791171875
7 1.1817578125
8 0.98646484375
9 1.08411132812
10 1.03528808594
11 1.01087646484
12 0.998670654297
13 1.00477355957
14 1.00172210693
15 1.00019638062
16 0.999433517456
17 0.999814949036
18 1.00000566483
19 0.999910306931
20 0.999957985878
21 0.999981825352
22 0.999993745089
23 0.999999704957
24 1.00000268489
#+end_example

* Open intervals
For some problems one can guarantee that the root will lie in a closed
interval $[a,b]$, and our algorithms above are well suited to this
circumstance.  But in other situations we may only be able to
guarantee that the root lies in an open or half-closed interval, such
as $(0,\infty)$. 

For this case, we supply as arguments the function $f$ and a triple $x$,
as with =root_interval=.  But in this case the elements of $x$ include
first and last the endpoints of the possibly /open/ interval, while
the middle element is an initial guess at the root of $f$.
#+name: root_in_open_interval
#+begin_src python :noweb no-export :exports code
from sys import float_info

<<newton>>

def root_in_open_interval(f,x,middle=middle):
    (a,x0,b)=x
    if b==Inf: b=float_info.max
    radius=[min(x0-a,b-x0)]*2
    xc=[x0-radius[0]/2.,x0,x0+radius[1]/2.] # Closed interval
    while sign(f(xc[0]))==sign(f(xc[-1])): # No root in closed interval; expand toward bounds
        if xc[0]-a < radius[0]:
            xc[0]=(a+xc[0])/2.
        else:
            radius[0]+=radius[0]
            xc[0]=x0-radius[0]/2.
        if b-xc[-1] < radius[1]:
            xc[-1]=(a+xc[-1])/2.
        else:
            radius[1]+=radius[1]
            xc[-1]=x0+radius[1]/2.

    return root_interval(f,xc,middle)
#+end_src

#+name: open_interval_example
#+begin_src python :noweb no-export :exports results :results output
<<root_in_open_interval>>

f=lambda x: 1./x-1
seq=root_in_open_interval(f,[0,10,Inf],middle=middle)
for i in range(25):
    x=seq.next()
    print i,x[1]
#+end_src

#+results: open_interval_example
#+begin_example
0 25.3125
1 12.96875
2 6.796875
3 3.7109375
4 2.16796875
5 1.396484375
6 1.0107421875
7 0.81787109375
8 0.914306640625
9 0.962524414062
10 0.986633300781
11 0.998687744141
12 1.00471496582
13 1.00170135498
14 1.00019454956
15 0.999441146851
16 0.999817848206
17 1.00000619888
18 0.999912023544
19 0.999959111214
20 0.999982655048
21 0.999994426966
22 1.00000031292
23 0.999997369945
24 0.999998841435
#+end_example

* Specifying precision
The =root_interval= and =root_in_open_interval= algorithms presented
above each generate a sequence of intervals which we expect to
converge to the single root of $f$.  But in practice we'd often like
to specify a precision and get back a number, rather than a convergent
sequence.  The following code provides this facility, using the
=newton= algorithm for selecting =middle=.
#+name: root_with_precision
#+begin_src python :noweb no-export :exports code :tangle ./consumerdemands/root_with_precision.py
<<root_in_open_interval>>

def root_with_precision(f,axb,tol,open_interval=False,middle=middle):
    if open_interval:
        seq=root_in_open_interval(f,axb,middle)
    else:
        seq=root_interval(f,axb,middle)
    x = next(seq)
    i=0
    while abs(x[0]-x[-1])>tol:
        x = next(seq)
        i+=1
        if i>1000: 
            warn("Tolerance is set to %.2E.  Change in value is %.2E.  Iterations are %d.  Perhaps tolerance is too high?" % (tol,x[0]-x[-1],i))
            return x[1]

    return x[1]
#+end_src

#+NAME: gee-tennis-connecticut-jersey
#+begin_src ipython :noweb no-export :exports results :results output
<<root_with_precision>>

print(root_with_precision(lambda x: 1./x-1,[0,100,Inf],1e-12,open_interval=True))

#+end_src



