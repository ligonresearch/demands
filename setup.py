from distutils.core import setup
import sys

setup(
    name='ConsumerDemands',
    author='Ethan Ligon',
    author_email='ligon@berkeley.edu',
    packages=['consumerdemands',],
    license='Creative Commons Attribution-Noncommercial-ShareAlike 4.0 International license',
    description='Tools for computing general demand systems from primitives.',
    url='https://bitbucket.org/ligonresearch/demands',
    long_description=open('README.txt').read(),
    setup_requires = ['pytest_runner'],
    tests_require = ['pytest']
)
